note
	description: "Summary description for {ELEVATOR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"
	explicit: wrapping
	model: number_of_floors, cabin_moving, cabin_last_visited, cabin_is_door_open

class
	ELEVATOR

create
	make
	--make_button

--invariant
--	cabin_last_visited >= 0

feature {NONE}-- Variables
	number_of_floors : INTEGER
	set_floor_buttons_up : MML_SET[INTEGER]

	cabin_moving : INTEGER
	cabin_last_visited : INTEGER
	cabin_is_door_open : BOOLEAN
	cabin_buttons_pressed : MML_SET[INTEGER]

	path : MML_SEQUENCE[INTEGER]

	--current_floor, top_floor: INTEGER
	--motor_direction: INTEGER -- -1 is down, 1 is up, 0 is stop
	--doors: BOOLEAN -- true is open, false is closed
	--mode: INTEGER -- -1 is descend, 1 is ascend, 0 is rest
	--destination_buttons: ARRAY[BOOLEAN]
	--up_call: ARRAY[BOOLEAN]
	--down_call: ARRAY[BOOLEAN]
	--tick_length: INTEGER
	--busy: BOOLEAN



feature {ANY} -- CABIN
	make (n_floors: INTEGER; i_last_floor_visited : INTEGER; i_moving : INTEGER; i_is_door_open : BOOLEAN)

	note
		status: creator

		require
			n_floors > 1
			i_last_floor_visited >= 0
			i_last_floor_visited < n_floors
			n_floors - i_last_floor_visited <= i_moving
			i_moving <= 1 - i_last_floor_visited

		do
			--unwrap
			cabin_last_visited := i_last_floor_visited
			cabin_moving := i_moving
			cabin_is_door_open := i_is_door_open
			number_of_floors := n_floors

		ensure
			number_of_floors = n_floors
			cabin_last_visited > 0
			cabin_last_visited <= number_of_floors
			number_of_floors - cabin_last_visited <= cabin_moving
			cabin_moving <= 1 - cabin_last_visited
			cabin_is_door_open = i_is_door_open

		end


feature {NONE}--CABIN

	-- move_up
	move_up
	require
		cabin_moving <= 1 - cabin_last_visited
		cabin_moving >=  number_of_floors - cabin_last_visited
		cabin_last_visited > 0
		cabin_last_visited <= number_of_floors - 1
		modify_field ("cabin_last_visited", Current)
		modify_field ("cabin_moving", Current)
	do
		cabin_last_visited := cabin_last_visited + 1
		cabin_moving := cabin_moving - 1
	ensure
		old cabin_last_visited + 1 = cabin_last_visited
		old cabin_moving - 1 = cabin_moving
	end

	-- move_down
	move_down

	require
		cabin_moving <= 1 - cabin_last_visited
		cabin_moving >=  number_of_floors - cabin_last_visited
		cabin_last_visited > 1
		cabin_last_visited <= number_of_floors
		modify_field ("cabin_last_visited", Current)
		modify_field ("cabin_moving", Current)
	do
		cabin_last_visited := cabin_last_visited - 1
		cabin_moving := cabin_moving + 1
	ensure
		old cabin_last_visited - 1 = cabin_last_visited
		old cabin_moving + 1 = cabin_moving
	end


	-- door_open
	door_open

	require
		cabin_is_door_open = false
		cabin_moving = 0
		modify_field ("cabin_moving", Current)
	do
		cabin_is_door_open := true
	ensure
		old cabin_is_door_open = not cabin_is_door_open
	end

	-- door_close
	door_close

	require
		cabin_is_door_open = true
		cabin_moving = 0
		modify_field ("cabin_moving", Current)
	do
		cabin_is_door_open := false
	ensure
		old cabin_is_door_open = not cabin_is_door_open
	end


--feature -- BUILDING

--feature -- CONTROL

--feature -- BUTTON


end
