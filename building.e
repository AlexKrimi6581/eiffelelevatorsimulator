note
	description: "Summary description for {BUILDING}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"
	model: number_of_floors, floors_arr

class
	BUILDING

create
	make

feature -- Variables
	number_of_floors : INTEGER
	floors_arr :attached SIMPLE_ARRAY[FLOOR]

feature -- constructor

	make (floors : INTEGER)


	require
		floors > 2
	local
		i : INTEGER
		f : FLOOR
	do
		number_of_floors := floors
		create floors_arr.make (number_of_floors)
		--create floors_arr.make_empty

		--create floors_arr.make (create {FLOOR}.make_floor, 1, number_of_floors)

		from
			i := 1
		invariant
			in_bounds: 0 < i and i <= number_of_floors
		--	not_empty: floors_arr.count>0
		until
			i = number_of_floors
		loop
		--	floors_arr[i].make_floor
		--	floors_arr.put (create {FLOOR}.make_floor, i)
			floors_arr.force (create {FLOOR}.make_floor() , i)
		--	floors_arr[i].make_floor
		variant
			number_of_floors - i
		end

	ensure
		number_of_floors = floors
	end

end
