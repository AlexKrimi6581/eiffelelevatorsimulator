note
	description: "Summary description for {BUTTON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"
	model: is_pressed
	--explicit: wrapping

class
	BUTTON

create
	make_button

feature -- Variables
	is_pressed : BOOLEAN

feature -- constructor

	make_button(press : BOOLEAN)

	require
		--modify(Current)
	do
		is_pressed := press
	ensure
		Current.is_pressed = press
	end

feature

	bn_press

	require
		Current.is_pressed = false
		--modify(Current)
	do
		is_pressed := true
	ensure
		old Current.is_pressed = not Current.is_pressed
	end

feature

	bn_unpress

	--note
		--status: skip
	require
		Current.is_pressed = true
		--modify(Current)
	do
		is_pressed := false
	ensure
		old Current.is_pressed = not Current.is_pressed
	end


end
