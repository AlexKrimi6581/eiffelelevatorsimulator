note
	description: "Summary description for {BUTTON_FLOOR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"
	model: value_up_down

class
	BUTTON_FLOOR

inherit
	BUTTON

create
	make_bn_floor

feature -- Variables
	value_up_down : BOOLEAN


feature -- constructor

	make_bn_floor (up_or_down : BOOLEAN)

	require
		--modify(Current, is_pressed)
	do
		--Current.make (false)
		value_up_down := up_or_down

	ensure

		Current /= Void
		value_up_down = up_or_down
	end


end
