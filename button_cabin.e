note
	description: "Summary description for {BUTTON_CABIN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BUTTON_CABIN

inherit
	BUTTON

create
	make_cabin_button

feature -- Variables
	number : INTEGER

feature -- constructor

	make_cabin_button(n: INTEGER; max : INTEGER)

	require
		n >= 1
		n <= max
	do
		number := n
	ensure
		number = n
	end



end
