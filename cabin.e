note
	description: "Summary description for {CABIN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"
	--model: is_moving, last_floor_visited, is_door_open

class
	CABIN

create
	make

feature -- Variables

	moving : INTEGER -- 0:NotMoving, >0:MovingUp, <0:MovingDown
	last_floor_visited : INTEGER
	is_door_open : BOOLEAN

feature

	make (i_last_floor_visited : INTEGER; i_moving : INTEGER; i_is_door_open : BOOLEAN)

	require
		i_last_floor_visited > 0
	do
		last_floor_visited := i_last_floor_visited
		moving := i_moving
		is_door_open := i_is_door_open
		--number_of_floors := floors
	ensure
		--number_of_floors = floors
	end

feature -- move_up

	--is_moving : INTEGER -- 0:NotMoving, >0:MovingUp, <0:MovingDown
	--last_floor_visited : INTEGER
	--is_door_open : BOOLEAN

	move_up--(last_floor_visited : INTEGER; is_moving : INTEGER; is_door_open : BOOLEAN)

	require
		moving >= 0
		--last_floor_visited + 1 <= floors_available
	do
		last_floor_visited := last_floor_visited + 1
		moving := moving - 1
	ensure
		old last_floor_visited + 1 = last_floor_visited
	end

feature -- move_down

	--is_moving : INTEGER -- 0:NotMoving, >0:MovingUp, <0:MovingDown
	--last_floor_visited : INTEGER
	--is_door_open : BOOLEAN

	move_down--(last_floor_visited : INTEGER; is_moving : INTEGER; is_door_open : BOOLEAN)

	require
		moving <= 0
		--last_floor_visited + 1 <= floors_available
	do
		last_floor_visited := last_floor_visited - 1
		moving := moving + 1
	ensure
		old last_floor_visited -1 = last_floor_visited
	end


feature -- door_open

	--is_moving : INTEGER -- 0:NotMoving, >0:MovingUp, <0:MovingDown
	--last_floor_visited : INTEGER
	--is_door_open : BOOLEAN

	door_open--(last_floor_visited : INTEGER; is_moving : INTEGER; is_door_open : BOOLEAN)

	require
		is_door_open = false
		moving = 0
	do
		is_door_open := true
	ensure
		old is_door_open = not is_door_open
	end

feature -- door_close

	--is_moving : INTEGER -- 0:NotMoving, >0:MovingUp, <0:MovingDown
	--last_floor_visited : INTEGER
	--is_door_open : BOOLEAN

	door_close--(last_floor_visited : INTEGER; is_moving : INTEGER; is_door_open : BOOLEAN)
	require
		is_door_open = true
		moving = 0
	do
		is_door_open := false
	ensure
		old is_door_open = not is_door_open
	end


end
