note
	description: "Summary description for {FLOOR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"
	--explicit: wrapping
	model: bn_up, bn_down

class
	FLOOR

create
	make_floor

feature -- VARIABLES
	bn_up : attached  BUTTON_FLOOR
	bn_down : attached  BUTTON_FLOOR

feature --constuctor
	make_floor
	require
	do
		create bn_up.make_bn_floor (true)
		create bn_down.make_bn_floor(false)
	ensure
		bn_up /= Void
		bn_down /= Void
	end

--feature	--try


	--try
		--local
			--other: detachable BUTTON_FLOOR
		--do
			--if other /= Void then Io.put_integer(other.is_pressed.to_integer) end
			--if attached bn_up as u then Io.put_integer (u.is_pressed.to_integer) end

		--end



end
