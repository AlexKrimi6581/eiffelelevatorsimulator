note
	description : "Elevator application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION
inherit
	EXECUTION_ENVIRONMENT

create
	make

feature {NONE} -- Initialization

	floors_in_building : INTEGER


	MY_CURRENT_POSITION : INTEGER

	make
		do
			MY_CURRENT_POSITION := 1;
			print("Press 1 to start simulation or 0 to exit%N")
			Io.read_integer()
			if Io.last_integer = 0 then
				print("Congratulations! You win this game!%N")
			else
				print("Plese specify the number of floors%N")
				Io.read_integer()
				if Io.last_integer > 2 then -- construt building
					create building.make(Io.last_integer)
					print("-")
					--Io.put_integer(building.floors_arr[1].bn_down.is_pressed.to_integer)
					print("-")
					create cabin.make (1, 0, false)
					Io.put_integer (building.number_of_floors)
				end
				print("Where you go? 1-up, 0-down %N")
				Io.read_integer()
				if Io.last_integer >= 0 or Io.last_integer <= 1 then -- call the cabin and enter
					print("Ok, waiting for the elevator to appear%N")
					-- TODO: add the function of elevator actually coming
					---

					-- cabin.current position == my position
					cabin.door_open()
					print("door opened%N")

					---
					print("Cool you are in the cabin%N")
					---
					cabin.door_close()
					print("door closed%N")
				end
				print("Which floor would you like to go?%N")
				Io.read_integer()
				if Io.last_integer >= 0 or Io.last_integer <= 1 then

				end
			end

			print("%N_________Test completed___________%N");
		end

feature
	building : BUILDING
	cabin : CABIN
	elevator : ELEVATOR
end
